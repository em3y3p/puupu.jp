import React from "react"
import { Link, graphql } from "gatsby"
import { StaticImage } from "gatsby-plugin-image"

import "./header.css"

export default () => (
  <header>
    <h1 class="header-sitename">
        <Link to={"/"}>
          <StaticImage
            src="../images/sitename-pic.png"
            height={44}
            alt="sitename picture"
          />
        </Link>
    </h1>
    <nav class="header-menu">
      <ul>
        <li><Link to={"/"}>HOME</Link></li>
        <li><Link to={"/about/"}>about</Link></li>
        <li><Link to={"/blog/"}>BLOG</Link></li>
        <li><Link to={"/blog/category/"}>category</Link></li>
        <li><Link to={"/blog/tag/"}>tag</Link></li>
      </ul>
    </nav>
  </header>
)