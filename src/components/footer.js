import React from "react"
import { Link, graphql } from "gatsby"
import { StaticImage } from "gatsby-plugin-image"

import "./footer.css"

export default () => (
  <footer class="footer">
    <h1 class="footer-sitename">
      <Link to={"/"}>
        <StaticImage
          href="/"
          src="../images/sitename-pic.png"
          height={29}
          alt="sitename picture"
        />
      </Link>
    </h1>
  </footer>
)