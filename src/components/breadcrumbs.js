import React from "react"
import { useStaticQuery, graphql ,Link} from "gatsby"
import { StaticImage } from "gatsby-plugin-image"

import "./breadcrumbs.css"

const Breadcrumbs = () => {
    const data = useStaticQuery(graphql`
    query {
      site {
        siteMetadata {
          author {
            name
            summary
          }
          social {
            twitter
          }
        }
      }
    }
  `)

const author = data.site.siteMetadata?.author

return (
    <div class="breadcrumbs">
      <ul>
        <li><Link to={"/"}>HOME</Link>　/　</li>
        
        <li><Link to={"/blog/"}>BLOG</Link></li>
      </ul>
    </div>
  )
}

export default Breadcrumbs