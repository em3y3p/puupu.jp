import * as React from "react"
import { Link } from "gatsby"
import Header from "../components/header"
import Footer from "../components/footer"

const Layout = ({ location, title, children }) => {
  const rootPath = `${__PATH_PREFIX__}/`
  const isRootPath = location.pathname === rootPath
  let header

  if (isRootPath) {
    header = (
      <h1 className="main-heading">
        <Link to="/">{title}</Link>
      </h1>
    )
  } else {
    <Header />
  }

  return (
    <div className="global-wrapper" data-is-root-path={isRootPath}>
      <Header/>
      <main>{children}</main>
      <Footer />
    </div>
  )
}

export default Layout
