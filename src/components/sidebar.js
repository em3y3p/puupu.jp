import React from "react"
import { useStaticQuery, graphql ,Link} from "gatsby"
import { StaticImage } from "gatsby-plugin-image"

import "./sidebar.css"

const Sidebar = () => {
    const data = useStaticQuery(graphql`
    query {
      site {
        siteMetadata {
          author {
            name
            summary
          }
          social {
            twitter
          }
        }
      }
    }
  `)

const author = data.site.siteMetadata?.author

return (
    <div class="sidebar">
      <h3>PROFILE</h3>
        <div class="profile">
          <div class="image-name">
            <StaticImage
                class="bio-avatar"
                src="../images/profile-pic.png"
                width={80}
                height={80}
                quality={95}
                alt="Profile picture"
            />
            <div class="profile-text">
            <Link to={"/about/"}>{author?.name && (
                <p>
                {author.name}
                </p>
            )}</Link>
            <p>服好きの20代OL</p>
            <p>シンプル×フレンチ</p>
          </div>
          </div>
        </div>
    </div>
  )
}

export default Sidebar